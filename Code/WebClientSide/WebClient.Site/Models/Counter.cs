﻿using System;

namespace MyWebClient.Site.Models
{
    public class Counter
    {
        public int Value { get; private set; }
        public DateTime LastUpdate { get; private set; }

        public Counter(int value)
        {
            Value = value;
            LastUpdate = DateTime.Now;
        }
    }
}