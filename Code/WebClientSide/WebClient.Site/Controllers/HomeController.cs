﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWebClient.Site.Models;

namespace MyWebClient.Site.Controllers
{
    public class HomeController : Controller
    {
        private static int _counter = 1;

        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC! ";
            ViewBag.Counter = new Counter(_counter);

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        
        public ActionResult Plus(int value)
        {
            if(value < 1)
                _counter++;
            else
            {
                _counter += value;
            }
            return Json(new Counter(_counter),JsonRequestBehavior.AllowGet);
        }

        public ActionResult Minus(int value)
        {
            if (value < 1)
                _counter--;
            else
            {
                _counter -= value;
            }
            return Json(new Counter(_counter),JsonRequestBehavior.AllowGet);
        }
    }
}
