﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartialViewsUsages.Models;

namespace PartialViewsUsages.Controllers
{
    public class MyDataController : Controller
    {
        private static List<Article> _articles = new List<Article>();
        private static List<Tag> _tags = new List<Tag>();
        private static List<Categorie> _categories = new List<Categorie>(); 

        static MyDataController()
        {
            _articles.Add(new Article(){Name = "article 1"});
            _articles.Add(new Article() { Name = "article 2" });
            _articles.Add(new Article() { Name = "article 3" });

            _tags.Add(new Tag(){Name = "T1"});
            _tags.Add(new Tag() { Name = "T2" });
            _tags.Add(new Tag() { Name = "T3" });

            _categories.Add(new Categorie(){ Name = "C1" });
            _categories.Add(new Categorie(){ Name = "C2" });

        }

        public ActionResult Index()
        {
            var articles = new ArticleViewModel(_articles,_tags);
            return View(articles);
        }

        [ChildActionOnly]
        public ActionResult GetCategories()
        {
            var categories = _categories;
            return PartialView(categories);
        }
    }
}
