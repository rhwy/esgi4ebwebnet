﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PartialViewsUsages.Models
{
    public class ArticleViewModel
    {
        public IEnumerable<Article> Articles { get; private set; }
        public IEnumerable<Tag> Tags { get; private set; }

        public ArticleViewModel(IEnumerable<Article> articles, IEnumerable<Tag> tags  )
        {
            Articles = articles;
            Tags = tags;
        }
    }
}