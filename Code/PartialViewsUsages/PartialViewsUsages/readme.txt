﻿# créer les modèles:

	public class Article
	{
		public string Name { get; set; }
	}

	public class Tag
	{
		public string Name { get; set; }
	}

	public class Categories
	{
		public string Name { get; set; }
	}


# créer un controlleur pour les tests:

	MyDataController

# créer des données statiques de test dans le controlleur

# créer les actions pour récupérer:
	- un nouveau modèle avec la liste des articles et la liste des tags
	- la liste des catégories

