﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdeasGroup.Domain;
using IdeasGroup.Models;

namespace IdeasGroup.Controllers
{
    public partial class IdeasGroupController : Controller
    {
        private readonly IIdeasGroupRepository _repository;

        //Si vous n'utilisez pas d'IOC, utilisez au moins la méthode
        //du POOR MANs DI : 
        //On fournit directement une version de l'implémentation
        //à utiliser avec le constructeur par défaut:
        //public IdeasGroupController() : this(new IdeaGroupRepository())
        //{ 
        //}

        public IdeasGroupController(IIdeasGroupRepository repository)
        {
            _repository = repository;
        }

        public virtual ActionResult Index()
        {
            var ideas = _repository
                .GetAll()
                .Select(idea => (IdeaListItem)idea);

            return View(ideas);
        }

        public virtual ActionResult Details(int id)
        {
            var idea = _repository.Find(x => x.Id == id);
            return View(idea);
        }

        [HttpGet]
        public virtual ActionResult Create()
        {
            var idea = new IdeaEditModel();
            return View(idea);
        } 

        [HttpPost]
        public virtual ActionResult Create(IdeaEditModel idea)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _repository.Save(idea.MergeTo(new Idea()));
                }
                else
                {
                    throw new InvalidDataException();
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(idea);
            }
        }
        
        [HttpGet]
        public virtual ActionResult Edit(int id)
        {
            var i = _repository.Find(x => x.Id == id);
            if(i==null)
                return RedirectToAction("Index");

            var ideaModel = new IdeaEditModel(i.Id, i.Title, i.Detail, i.Published, i.Author);
            return View(ideaModel);
        }

        [HttpPost]
        public virtual ActionResult Edit(IdeaEditModel idea)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var trackedItem = _repository.Find(x => x.Id == idea.Id);
                    if(trackedItem == null)
                        throw new InvalidDataException();

                    _repository.Save(idea.MergeTo(trackedItem));
                }
                else
                {
                    throw new InvalidDataException();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(idea);
            }
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            var idea = _repository.Find(x => x.Id == id);
            return View(idea);
        }

        [HttpPost]
        public virtual ActionResult Delete(Idea idea)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _repository.Delete(idea);
                }
                else
                {
                    throw new InvalidDataException();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
