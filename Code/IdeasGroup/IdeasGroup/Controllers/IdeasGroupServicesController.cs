﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdeasGroup.Domain;

namespace IdeasGroup.Controllers
{
    public class IdeasGroupServicesController : Controller
    {
        private readonly IIdeasGroupRepository _repository;

        public IdeasGroupServicesController(IIdeasGroupRepository repository)
        {
            _repository = repository;
        }

        public ActionResult VoteUp(int ideaId)
        {
            
            return Json(new Counter(_counter), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Minus(int ideaId)
        {
            var idea = _repository.Find(x => x.Id == ideaId);
            if (idea != null)
            {
                idea.Votes.VoteDown();
                return Json(idea.Votes, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
