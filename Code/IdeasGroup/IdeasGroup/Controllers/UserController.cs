﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdeasGroup.Models;

namespace IdeasGroup.Controllers
{
    public partial class UserController : Controller
    {
        private static List<UserViewModel> _userList = new List<UserViewModel>();
        public virtual ActionResult Index()
        {
            var list = _userList;
            return View(list);
        }

        //
        // GET: /User/Details/5

        public virtual ActionResult Details(int id)
        {
            var user = _userList.Where(x => x.Id == id).SingleOrDefault();

            return View(user);
        }

        //
        // GET: /User/Create

        public virtual ActionResult Create()
        {
            var user = new UserViewModel();
            return View(user);
        } 

        //
        // POST: /User/Create

        [HttpPost]
        public virtual ActionResult Create(UserViewModel user)
        {
            try
            {
                _userList.Add(user);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /User/Edit/5

        public virtual ActionResult Edit(int id)
        {
            var user = _userList.SingleOrDefault(x => x.Id == id);
            return View(user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(int id, UserViewModel user)
        {
            try
            {
                _userList.Remove(_userList.SingleOrDefault(x => x.Id == id));
                _userList.Add(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /User/Delete/5

        public virtual ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /User/Delete/5

        [HttpPost]
        public virtual ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _userList.Remove(_userList.SingleOrDefault(x => x.Id == id));
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
