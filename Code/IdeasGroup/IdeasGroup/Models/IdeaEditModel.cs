﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using IdeasGroup.Domain;

namespace IdeasGroup.Models
{
    public class IdeaEditModel
    { 
        [ScaffoldColumn(false)]
        public int Id { get; set; }
 
        [Required]
        [MaxLength(255, ErrorMessage = "Your title should be less than 255 chars")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Detail { get; set; }

        [DataType(DataType.DateTime)]
        [ScaffoldColumn(false)]
        public DateTime? Published { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MaxLength(32,ErrorMessage = "The author name should be less than 32 chars")]
        public string Author { get; set; }

        public IdeaEditModel()
        {
            
        }

        public IdeaEditModel(int id,string title, string detail, DateTime published, string author)
        {
            Id = id;
            Title = title;
            Detail = detail;
            Published = published;
            Author = author;
        }

        public Idea MergeTo(Idea source)
        {
            source.Title = Title;
            source.Detail = Detail;
            if (Published != null) source.Published = Published.Value;
            source.Author = Author;
            return source;
        }

    }
}