﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IdeasGroup.Domain;

namespace IdeasGroup.Models
{
    /// <summary>
    /// ViewModel used to present an Idea in a list
    /// </summary>
    public class IdeaListItem
    {
        public int Id { get; private set; }
        public string Title { get; private set; }
        public DateTime Published { get; private set; }
        public string Author { get; private set; }
        public int Comments { get; private set; }
        public int VotesUp { get; private set; }
        public int VotesDown { get; private set; }

        public IdeaListItem(int id, string title, DateTime published, string author, int comments, int votesUp, int votesDown)
        {
            VotesDown = votesDown;
            VotesUp = votesUp;
            Comments = comments;
            Author = author;
            Published = published;
            Title = title;
            Id = id;
        }

        public static explicit operator IdeaListItem(Idea source)
        {
            return new IdeaListItem(
                source.Id,
                source.Title,
                source.Published,
                source.Author,
                source.Comments.Count,
                source.Votes.Up,
                source.Votes.Down);
        }
    }
}