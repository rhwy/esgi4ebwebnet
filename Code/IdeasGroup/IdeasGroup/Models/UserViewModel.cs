﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IdeasGroup.Models
{
    public class UserViewModel
    {

        [Required]
        [ScaffoldColumn(false)]
        public int Id { get; set; }


        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Pass { get; set; }


        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Confirmez votre adresse e-mail")]
        [Compare("Email", ErrorMessage = "Les deux adresses e-mail sont différentes")]
        [DataType(DataType.EmailAddress)]
        public string Emailconfirmation { get; set; }

        [Display(Name = "Date de création")]
        [DataType(DataType.DateTime)]
        public DateTime CreationDate { get; set; }

        [DataType(DataType.MultilineText)]
        public string PersonalInformation { get; set; }

        [DataType(DataType.Url)]
        public string BlogUrl { get; set; }

        
    }
}