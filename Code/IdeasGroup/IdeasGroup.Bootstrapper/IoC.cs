using IdeasGroup.Domain;
using IdeasGroup.Infrastructure.EF;
using StructureMap;
namespace IdeasGroup.Bootstrapper {
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IIdeasGroupRepository>().Use<IdeasGroupRepository>();
                        });
            return ObjectFactory.Container;
        }
    }
}