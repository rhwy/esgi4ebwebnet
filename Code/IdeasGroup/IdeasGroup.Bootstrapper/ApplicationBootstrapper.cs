﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Efmap;
using Efmap.Bootstrap;
using IdeasGroup.Infrastructure.EF;
using StructureMap;

namespace IdeasGroup.Bootstrapper
{
    public class ApplicationBootstrapper : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Root", // Route name
                "", // URL with parameters
                new { controller = "IdeasGroup", action = "Index"} // Parameter defaults
            );

            //Better route for printing one idea
            //Ensure id is numeric
            routes.MapRoute(
                "Idea", // Route name
                "idea/{id}/{title}", // URL with parameters
                new { controller = "IdeasGroup", action = "Details", title = UrlParameter.Optional },
                new { id = @"\d*" }
            );


            routes.MapRoute(
                "Ideas", // Route name
                "Ideas/{action}/{id}", // URL with parameters
                new { controller = "IdeasGroup", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
               "User", // Route name
               "users/{action}/{id}", // URL with parameters
               new { controller = "User", action = "Index", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
                "Home", // Route name
                "Home/{action}", // URL with parameters
                new { controller = "Home", action = "Index"} // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            var container = (IContainer)IoC.Initialize();
            DependencyResolver.SetResolver(new SmDependencyResolver(container));
            
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            InitModels();

        }

        protected void InitModels()
        {
            EntityMapHelper.SetEntityInitializer<IdeasGroupContext>(new IdeaEntityInitializer());
            
            //define the data class to use to add custom sql commands at 
            EntityMapHelper.SetDbInitializer<IdeasGroupContext>(new IdeaDbInitializer());

            //define the pattern structure to initialize database
            EntityMapHelper.Initialize<IdeasGroupContext>().With<DropCreateAlways<IdeasGroupContext>>();

        }
    }
}
