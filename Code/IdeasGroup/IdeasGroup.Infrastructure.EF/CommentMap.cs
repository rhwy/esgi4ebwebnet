﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Efmap.Helpers;
using IdeasGroup.Domain;

namespace IdeasGroup.Infrastructure.EF
{
    public class CommentMap : EntityTypeConfiguration<Comment>
    {
        public CommentMap()
            : base()
        {
            this.WithRequiredGeneratedIdentity(p => p.Id);
        }
    }
}
