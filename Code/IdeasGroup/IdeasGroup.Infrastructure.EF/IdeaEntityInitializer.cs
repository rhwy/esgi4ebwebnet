﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Efmap.Bootstrap;
using IdeasGroup.Domain;

namespace IdeasGroup.Infrastructure.EF
{
    public class IdeaEntityInitializer : EntityInitializer<IdeasGroupContext>
    {
        public IdeaEntityInitializer()
        {
            this.AddCommand(
                "add presentation idea",
                (context) =>
                {
                    var idea = new Idea
                                    {
                                        Title = "what about creating an ideas site?",
                                        Detail =
                                            @"Donec ullamcorper nulla non metus auctor fringilla. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum. Donec ullamcorper nulla non metus auctor fringilla. Donec sed odio dui. Aenean lacinia bibendum nulla sed consectetur.

Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.

Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Curabitur blandit tempus porttitor. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla.",
                                        Author = "admin"
                                    };
                    idea.Comments.Add(new Comment { Author = "admin", Content = "vote and add your comments about the idea" });
                    idea.Votes = new Votes(2,3);

                    context.Ideas.Add(idea);

                    var idea2 = new Idea
                                     {
                                         Title = "Create a school without teachers!",
                                         Detail =
                                             @"Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Curabitur blandit tempus porttitor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
",
                                         Author = "Rui"
                                     };
                    idea2.Comments.Add(new Comment { Author = "Joe", Content = "It should be great!" });
                    idea2.Comments.Add(new Comment { Author = "Diablo", Content = "Teachers are evil }:=)" });
                    idea2.Comments.Add(new Comment { Author = "Admin", Content = "I'm a teacher, and I don't like students,it's ok for me!" });
                    idea2.Votes = new Votes(4,2);
                    context.Ideas.Add(idea2);
                });
        }
    }
}
