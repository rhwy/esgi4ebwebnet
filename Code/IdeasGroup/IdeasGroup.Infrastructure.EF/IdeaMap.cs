﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Efmap.Helpers;
using IdeasGroup.Domain;

namespace IdeasGroup.Infrastructure.EF
{
    public class IdeaMap : EntityTypeConfiguration<Idea>
    {
        public IdeaMap()
            : base()
        {
            this.WithRequiredGeneratedIdentity(p => p.Id);
            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(100);
            Property(p => p.Votes.Up)
                .HasColumnType("int")
                .HasColumnName("VoteUp");
            Property(p => p.Votes.Down)
                .HasColumnType("int")
                .HasColumnName("VoteDown");
            ToTable("Ideas");
        }
    }
}
