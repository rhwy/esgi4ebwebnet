﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Efmap.Helpers;
using IdeasGroup.Domain;

namespace IdeasGroup.Infrastructure.EF
{
    public class IdeasGroupContext : DbContext
    {
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Idea> Ideas { get; set; }

        public IdeasGroupContext()
            : base("IdeasGroupDb")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.AutoLoadForThisContext(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }
    }
}
