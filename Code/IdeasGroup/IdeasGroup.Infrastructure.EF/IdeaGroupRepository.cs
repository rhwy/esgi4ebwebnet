﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using IdeasGroup.Domain;

namespace IdeasGroup.Infrastructure.EF
{
    public class IdeasGroupRepository : IIdeasGroupRepository
    {
        private IdeasGroupContext _db = new IdeasGroupContext();

        private IQueryable<Idea> getRootQuery()
        {
            return _db.Ideas.Where(q => q.Active).OrderByDescending(x => x.Published);
        }
        public IEnumerable<Idea> GetAll()
        {
            return getRootQuery().Include(q => q.Comments).AsEnumerable();
        }

        public IEnumerable<Idea> Get(System.Linq.Expressions.Expression<Func<Idea, bool>> filter)
        {
            return getRootQuery().Include(q => q.Comments).Where(filter);
        }

        public Idea Find(System.Linq.Expressions.Expression<Func<Idea, bool>> filter)
        {
            return getRootQuery().FirstOrDefault(filter);
        }

        public void Save(Idea idea)
        {
            Idea previous = Find(x => x.Id == idea.Id);
            if (previous == null)
            {
                _db.Ideas.Add(idea);
            }
            else
            {
                previous = idea;
            }
            _db.SaveChanges();
        }

        public void Delete(Idea idea)
        {
            Idea previous = Find(x => x.Id == idea.Id);
            if (previous != null)
            {
                _db.Ideas.Remove(idea);
            }
        }


    }
}
