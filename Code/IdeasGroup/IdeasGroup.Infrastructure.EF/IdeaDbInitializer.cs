﻿using Efmap.Bootstrap;

namespace IdeasGroup.Infrastructure.EF
{
    public class IdeaDbInitializer : DbInitializer
    {
        public IdeaDbInitializer()
        {
            //add constraint to avoid exact duplicate ideas
            this.AddCommand(
                "IF NOT ISNULL(OBJECT_ID('Ideas','U'),0)=0 ALTER TABLE Ideas ADD CONSTRAINT UN_Ideass_Title UNIQUE(Title)"
            );
        }
    }
}
