﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using IdeasGroup.Bootstrapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IdeasGroup;
using IdeasGroup.Controllers;
using MvcContrib.TestHelper;

namespace IdeasGroup.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        
        [TestMethod]
        public void VerifyRoutes()
        {
            ApplicationBootstrapper.RegisterRoutes(RouteTable.Routes); 

            "~/".WithMethod(HttpVerbs.Get).ShouldMapTo<HomeController>(x => x.Index());

            "~/Ideas/Details/1".WithMethod(HttpVerbs.Get).ShouldMapTo<IdeasGroupController>(x => x.Details(1));
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Welcome to ASP.NET MVC!", result.ViewBag.Message);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
