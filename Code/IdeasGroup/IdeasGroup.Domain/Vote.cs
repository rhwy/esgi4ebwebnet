﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasGroup.Domain
{
    public class Votes
    {
        public int Up { get; private set; }
        public int Down { get; private set; }
        public int TotalVotes
        {
            get { return Up + Down; }
        }
        public Votes()
        {
            
        }
        public Votes(int up, int down)
        {
            Up = up;
            Down = down;
        }

        public int VoteUp()
        {
            return Up++;
        }

        public int VoteDown()
        {
            return Down++;
        }

    }
}
