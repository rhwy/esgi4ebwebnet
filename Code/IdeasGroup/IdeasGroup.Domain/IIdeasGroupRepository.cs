﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace IdeasGroup.Domain
{
    public interface IIdeasGroupRepository
    {
        IEnumerable<Idea> GetAll();
        IEnumerable<Idea> Get(Expression<Func<Idea, bool>> filter);
        Idea Find(Expression<Func<Idea, bool>> filter);
        void Save(Idea idea);
        void Delete(Idea idea);
    }
}
