﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasGroup.Domain
{
    public class Idea
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public DateTime Published { get; set; }
        public string Author { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual Votes Votes { get; set; }
        public bool Active { get; set; }
        public Idea()
        {
            Comments = new HashSet<Comment>();
            Votes = new Votes();
            Published = DateTime.Now;
            Active = true;
        }
    }
}
