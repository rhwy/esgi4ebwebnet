﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IdeasGroup.Domain
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Published { get; set; }
        public string Author { get; set; }

        public Comment()
        {
            Published = DateTime.Now;
        }
    }
}
